import { breakTextIntoP } from "./utils";
import { text } from "./data";

describe('util', () => {
  describe('breakTextIntoP should', () => {
    it('break text into many paragraphs using "\n" as splitter', () => {
      expect(Object.keys(breakTextIntoP(text))).toHaveLength(9);
    })

    it('break into an object indexed by paragraph id', () => {
      const p = breakTextIntoP(text)[0];
      expect(p.id).toEqual(0);
      expect(p.text).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat felis id lorem cursus malesuada. Fusce fringilla nunc in laoreet venenatis. Sed efficitur lacus non urna cursus, sed feugiat est imperdiet. Vestibulum leo justo, bibendum ut lorem nec, lacinia lacinia sem. Integer sit amet magna est. Sed nec magna ac felis aliquam lobortis. Nam viverra ultricies ante, quis consequat lorem aliquet quis. Aliquam bibendum sollicitudin tellus ac porttitor. Fusce et leo efficitur, facilisis metus non, suscipit quam. Duis eget metus quis magna dictum elementum eget et turpis. Nunc hendrerit scelerisque sapien vitae varius. Phasellus dolor magna, posuere sed hendrerit id, faucibus et eros.');
    })
  });
});
