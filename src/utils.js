import { Map } from 'immutable';

export const breakTextIntoP = text => Map(text.split('\n').map((p, i) => [i, Map({
  id: i,
  text: p,
})])).toJS();
