import React, { Component } from 'react';
import './App.css';
import ReactDOMServer from 'react-dom/server';
import { text } from './data';
import { breakTextIntoP } from './utils';

const TaggedText = ({ tag, onClick }) => (
  <span className="App-text-tagged" onClick={() => onClick(tag)}>
    {tag.text}
  </span>
);

const getParagraphWithSelections = ({ text, tags, onClick }) => {
  debugger
  if (!tags || !tags.length) return text;
  const nextText = tags.reduce((acc, tag) => {
    return acc.replace(tag.text, ReactDOMServer.renderToString(<TaggedText onClick={onClick} tag={tag} />))
  }, text);
  return nextText;
}

const Paragraph = ({ children, ...props}) => (
  <p {...props} dangerouslySetInnerHTML={{
  __html : children,
  }} />
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: [{
        anchorOffset: 257,
        extentOffset: 269,
        pId: '6',
        text: 'Pellentesque',
      }],
      selection: null,
      text: breakTextIntoP(text),
    }
  }

  handleAddButtonClick = () => {
    debugger
    this.setState(state => {
      return {tags: state.tags.concat({ ...state.selection })};
    });
  }

  handleRemoveButtonClick = () => {
    this.setState(state => {
      return {tags: state.tags.filter(tag => tag.text !== state.selection.text)};
    });
  }

  handleTaggedTextClicked = event => {
    this.handleTextMouseUp(event);
  }

  handleTextMouseUp = event => {
    debugger
    const selection = window.getSelection();
    this.setState({
      selection: {
        pId: event.target.id,
        text: selection.toString(),
        anchorOffset: selection.anchorOffset,
        extentOffset: selection.extentOffset,
      }
    });
  }

  render() {
    return (
      <div className="App">
        <div>
          {Object.keys(this.state.text).map(key => {
            const pText = this.state.text[key];
            if (!pText.text) return <br key={key} />;
            return (
              <Paragraph key={key} id={key} onMouseUp={this.handleTextMouseUp}>
                {getParagraphWithSelections({ text: pText.text, tags: this.state.tags })}
              </Paragraph>
            );
          })}
        </div>
        <button onClick={this.handleAddButtonClick}>Add</button>
        <button onClick={this.handleRemoveButtonClick}>Remove</button>
      </div>
    );
  }
}

export default App;
